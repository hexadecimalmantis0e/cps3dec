#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <limits.h>

/* Why would ANSI C make me do this? */
typedef unsigned short u16;
#if UINT_MAX == 4294967295U
typedef unsigned int u32;
#else
typedef unsigned long u32;
#endif

#define ERRDIE errorout(__FILE__, __LINE__)

enum {
	REDEARTH,
	SFIII,
	JOJO,
	SFIII3,
	JOJOBA,
};

enum {
	BANKCOUNT = 4,
	SIMMCOUNT = 2,
	BANKSIZE = 0x200000,
	SIMMSIZE = BANKSIZE * BANKCOUNT,
	PROMSIZE = SIMMSIZE * SIMMCOUNT,
};

u32 keys[] = {
	0x9e300ab1, 0xa175b82c,
	0xb5fe053e, 0xfc03925a,
	0x02203ee3, 0x01301972,
	0xa55432b4, 0x0c129981,
	0x23323ee3, 0x03021972
};

struct {
	char n[sizeof("jojobaner1")];
	int i;
} idmap[] = {
	{ "redeartn", REDEARTH },
	{ "sfiiin", SFIII },
	{ "sfiiina", SFIII },
	{ "jojon", JOJO },
	{ "jojonr1", JOJO },
	{ "jojonr2", JOJO },
	{ "sfiii3n", SFIII3 },
	{ "sfiii3nr1", SFIII3 },
	{ "jojoban", JOJOBA },
	{ "jojobane", JOJOBA},
	{ "jojobanr1", JOJOBA},
	{ "jojobaner1", JOJOBA}
};
#define IDLEN (sizeof(idmap)/sizeof(idmap[0]))

void
usage(void)
{
	puts("Usage:");
	puts("cps3dec [-o | -n] [gameid] [out] [files]");
	puts("cps3dec -o [gameid] [out] [10 file] [20 file]");
	puts("cps3dec -n [gameid] [out] [simm1.0 file] [simm1.1 file] [simm1.2 file] [simm1.3 file] [simm2.0 file] [simm2.1 file] [simm2.2 file] [simm2.3 file]");
	exit(1);
}

void
errorout(char *file, int line)
{
	printf("%s:%d %s\n", file, line, strerror(errno));
	exit(errno);
}

void
frombe(u32 *d, int len)
{
	int i;
	len/=sizeof(u32);
	for(i=0;i<len;i++){
		unsigned char *b = (char *)(d+i);
		d[i] = b[3] | (b[2]<<8) | (b[1]<<16) | (b[0]<<24);
	}
}

void
tobe(u32 *d, int len)
{
	int i;
	len/=sizeof(u32);
	for(i=0;i<len;i++){
		u32 w = d[i];
		unsigned char *b = (char *)(d+i);
		b[0] = w>>24&0xff;
		b[1] = w>>16&0xff;
		b[2] = w>>8&0xff;
		b[3] = w&0xff;
	}
}

u16
lrot(u16 v, int n)
{
	u32 a = v>>(16-n);
	return ((v<<n)|a)%0x10000;
}

u16
rotxor(u16 v, u16 x)
{
	u16 r = v+lrot(v, 2);
	return lrot(r, 4)^(r&(v^x));
}

u32
cps3mask(u32 addr, u32 k1, u32 k2)
{
	u16 v;

	addr ^= k1;
	v = (addr&0xffff)^0xffff;
	v = rotxor(v, k2&0xffff);
	v ^= (addr>>16)^0xffff;
	v = rotxor(v, k2>>16);
	v ^= (addr&0xffff)^(k2&0xffff);
	return v | (v << 16);
}

long
fsize(FILE *f){
	long p, r;
	p = ftell(f);
	fseek(f, 0L, SEEK_END);
	r = ftell(f);
	fseek(f, p, SEEK_SET);
	return r;
}

void
loadoldrom(void *bp, char **fn){
	FILE *f[2];
	char *b = bp;

	if(!(f[0] = fopen(fn[0], "rb"))) ERRDIE;
	if(!(f[1] = fopen(fn[1], "rb"))) ERRDIE;
	if(fsize(f[0])+fsize(f[1]) != PROMSIZE){
		puts("ROM file sizes are incorrect! 10 and 20 should be 8MB each.");
		exit(3);
	}
	if(fread(b, 1, SIMMSIZE, f[0]) != SIMMSIZE) ERRDIE;
	if(fread(b+SIMMSIZE, 1, SIMMSIZE, f[1]) != SIMMSIZE) ERRDIE;
	fclose(f[0]);
	fclose(f[1]);
}

void
loadnewrom(void *bp, char **fn){
	FILE *f[BANKCOUNT];
	char *b = bp;
	int i;

	for(i=0; i<BANKCOUNT; i++){
		if(!(f[i] = fopen(fn[i], "rb"))) ERRDIE;
		if(fsize(f[i]) != BANKSIZE){
			puts("ROM file sizes are incorrect! Each simm file should be 2MB.");
			exit(3);
		}
	}
	for(i=0; i<PROMSIZE; i+=4){
		int j;
		for(j=0; j<BANKCOUNT; j++)
			if(fread(b+i+j, 1, 1, f[j]) != 1)
				if(ferror(f[j])) ERRDIE;
	}
	for(i=0; i<BANKCOUNT; i++) fclose(f[i]);
}
	
int
main(int argc, char *argv[])
{
	int i;
	FILE *in, *out;
	u32 *buf;
	int k1, k2;

	if(argc<4) usage();
	for(i=0; i<IDLEN; i++){
		if(!strcmp(argv[2], idmap[i].n)){
			int ki = idmap[i].i;
			k1 = keys[ki*2];
			k2 = keys[ki*2+1];
			goto found;
		}
	}
	puts("gameid must be one of: ");
	for(i=0; i<IDLEN-1; i++) printf("%s, ", idmap[i].n);
	printf("%s\n", idmap[i].n);
	return 2;

found:
	buf = calloc(PROMSIZE, 1);
	if(!strcmp(argv[1], "-o")){
		if(argc!=6) usage();
		loadoldrom(buf, argv+4);
	}
	else if(!strcmp(argv[1], "-n")){
		if(argc!=12) usage();
		loadnewrom(buf, argv+4);
	}
	else usage();
	frombe(buf, PROMSIZE);
	for(i=0; i<PROMSIZE; i+=4) buf[i/4]^=cps3mask(i+0x06000000, k1, k2);
	tobe(buf, PROMSIZE);
	if(!(out = fopen(argv[3], "wb"))) ERRDIE;
	if(fwrite(buf, 1, PROMSIZE, out) != PROMSIZE) ERRDIE;
	free(buf);
	fclose(out);
	return 0;
}
